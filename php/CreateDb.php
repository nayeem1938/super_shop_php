<?php


class CreateDb
{
        public $servername = "localhost";
        public $username = "root";
        public $password = "";
        public $dbname ="super";
        public $tablename;
        public $con;


        // class constructor
    public function __construct()
    {
      // create connection
        $this->con = mysqli_connect($servername, $username, $password);

        // Check connection
        if (!$this->con){
            die("Connection failed : " . mysqli_connect_error());
        }

        // query
        $sql = "CREATE DATABASE IF NOT EXISTS $dbname";

        // execute query
        if(mysqli_query($this->con, $sql)){

            $this->con = mysqli_connect($servername, $username, $password, $dbname);

            // sql to create new table
            $sql = "CREATE TABLE customer(
                    customer_id int PRIMARY KEY AUTO_INCREMENT,
                    customer_name VARCHAR(50),
                    phone VARCHAR(15),
                    email VARCHAR(50),
                    address VARCHAR(255),
                    password VARCHAR(15)
                    );

                    CREATE TABLE products(
                    product_id int PRIMARY KEY AUTO_INCREMENT,
                    product_name VARCHAR(50),
                    price DOUBLE,
                    stock INT,
                    image VARCHAR(255)
                    );

                    CREATE TABLE orders(
                    order_id int PRIMARY KEY AUTO_INCREMENT,
                    customer_id int,
                    total DOUBLE,
                    tax DOUBLE,
                    discount DOUBLE,
                    payabale DOUBLE,
                    FOREIGN KEY (customer_id) REFERENCES customer(customer_id)
                    );

                    CREATE TABLE order_line(
                    line_id int PRIMARY KEY AUTO_INCREMENT,
                    product_id int,
                    order_id int,
                    quantity int,
                    price DOUBLE,

                    FOREIGN KEY (product_id) REFERENCES products(product_id),
                    FOREIGN KEY (order_id) REFERENCES orders(order_id)
                    );";

            if (!mysqli_query($this->con, $sql)){
                echo "Error creating table : " . mysqli_error($this->con);
            }

        }
        
    }

    // get product from the database
    public function getData($query){
		$result = $this->connection->query($query);
		if(!$result){
			return false;
		}
			$rows = array();
		
			while($row = $result->fetch_assoc()){
				$rows[] = $row;
			}
		return $rows;
	}
    public function execute($query){
		$result = $this->connection->query($query);
		if($result == false){
            echo "not executed";
			return false;
		}else{
            echo "executed";
			return true;
		}
	}
	
	public function delete($id,$table_name){
		$query = "delete from $table_name where id=$id";
		
		$result = $this->connection->query($query);
		if($result == false){
			return false;
		}else{
			return true;
		}
	}
}
?>
