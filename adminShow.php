<?php
include('adminnav.php');
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	</head>
	<body class="bg-dark">
             <div class="container bg-light col-11" style="margin-top:20px;">
    </div>
    <div class="container col-12 " style="max-height: 350px">
        <div id="myCarousel" class="carousel slide col-12" style="max-height: 350px" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
                <li data-target="#myCarousel" data-slide-to="3"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active">
                    <img src="download%20(1).jpg" alt="Los Angeles" style="width:100%; max-height: 350px">
                </div>

                <div class="item">
                    <img src="download%20(2).jpg" alt="Chicago" style="width:100%;  max-height: 350px">
                </div>

                <div class="item">
                    <img src="download.jpg" alt="New york" style="width:100%;  max-height: 350px">
                </div>
                <div class="item">
                    <img src="super-shop-management-software.jpg" alt="york" style="width:100%;  max-height: 350px">
                </div>
            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
    <div>
        <h2 class="display-3 text-center text-white">Available Product</h2>
    </div>
            
            
		<div class="container">
			<?php
				$query = "SELECT * FROM products";
				$result = mysqli_query($link, $query);
				if(mysqli_num_rows($result) > 0)
				{
					while($row = mysqli_fetch_array($result))
					{
				?>
			<div class="col-md-4">
				<form method="post">
					<div style="border:1px solid #white;  border-radius:5px; padding:16px;" align="center">

                        <img src="productImages/<?php echo $row['image']; ?>" alt="Lights" style="height:250px;">

						<h4 class="text-info"><?php echo $row["product_name"]; ?></h4>

						<h4 class="text-info">TK <?php echo $row["price"]; ?></h4>

						<input type="hidden" name="hidden_name" value="<?php echo $row["product_name"]; ?>" />

						<input type="hidden" name="hidden_price" value="<?php echo $row["price"]; ?>" />
                    </div>
				</form>
				<div style="border:1px solid #white;  border-radius:5px; padding:16px;" align="center">
				<button class="btn bg-info"><a href="edit.php?id=<?php echo $row["product_id"]; ?>"><span class="" style="color:white">Update</span></a></button>
				<button class="btn bg-info"><a href="delete.php?id=<?php echo $row["product_id"]; ?>"><span class="text-danger">Delete</span></a></button>
                </div>
			</div>
			<?php
					}
				}
			?>
        </div>
	
	<br />
	    <div class="footer bg-light text-center h1 font-weight-lighter font">Copyright &copy; 2019 Nayeem. All Rights Reserved</div>

	</body>
</html>
