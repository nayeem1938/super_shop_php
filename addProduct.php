<?php
$link = mysqli_connect("localhost", "root", "", "supershop");
                            // Check connection
                            if($link === false){
                                die("ERROR: Could not connect. " . mysqli_connect_error());
                            }
                            else{
                                echo("conected");
                            }
                           if(isset($_POST['addProduct']))
                        {
                            $name=mysqli_real_escape_string($link,$_POST['inputProductName']);
                            $price =mysqli_real_escape_string($link,$_POST['inputProductPrice']);
                            $stock =mysqli_real_escape_string($link,$_POST['inputStock']);
                            //$image =mysqli_real_escape_string($link,$_POST['inputProductPrice']);
                        
       $file_Name = date("Y-m-d-H-i-s").$name.str_replace(" ", "_", $_FILES['image'] ['name']);
       $destination = "productImages/".$file_Name;
       $fileName = $_FILES['image']['tmp_name'];
       if(move_uploaded_file($fileName, $destination))
           {
                $query1=mysqli_query($link,"insert into products(product_name, price, stock, image) values('$name', '$price', '$stock', '$file_Name')");
                            //$query1=mysqli_query($link,"insert into products(product_name, price, stock) values('$name', '$price', '$stock')");
                            if($query1)
                            {        
                             echo '<script type="text/javascript">alert("Products added")</script>';   
                            }
                            else{
                                echo '<script type="text/javascript">alert("Failed")</script>';
                            }
                        }
                           }

?>
<!DOCTYPE html>
<html>

<head>
    <title>
        Add Product
    </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

</head>

<body class="bg-dark">
    <?php require_once("adminnav.php"); ?>
    <div style="margin-left=" 20px"; margin-right="70%" ; color="white" ; ">
        <form action="" style="color:white" method="post" enctype="multipart/form-data">
        <label for="inputProductName">Product Name: <input type="text" name="inputProductName" class="form-control" placeholder="Product Name"></label>
        
        <label for="inputProductPrice">Product Price: <input type="text" name="inputProductPrice" class="form-control" placeholder="Product Price"></label>
        
        <label for="inputStock">Stock: <input type="text" name="inputStock" class="form-control" placeholder="Stock"></label>
        
        <label for="inputProductImage">Product Image: <input type="file" name="image" placeholder="Product Image"></label>
        
        <input class="btn bg-primary" type="submit" name="addProduct">
        </form>

    </div>
</body>

</html>
