<?php
                            $link = mysqli_connect("localhost", "root", "", "supershop");
                        
                            if($link === false){
                                die("ERROR: Could not connect. " . mysqli_connect_error());          // Check connection
                            }
                            $id="";
                            session_start();
                            if(isset($_POST['registration']))
                            {
                            $name=mysqli_real_escape_string($link,$_POST['inputUserName']);
                            $phone=mysqli_real_escape_string($link,$_POST['inputPhone']);
                            $email=mysqli_real_escape_string($link,$_POST['inputEmail']);
                            $address =mysqli_real_escape_string($link,$_POST['inputAddress']);
                            $password =mysqli_real_escape_string($link,$_POST['inputPassword']);

                            $query1=mysqli_query($link,"insert into customer(customer_name, phone, email, address, password) values('$name', '$phone', '$email', '$address', '$password')");
                            if($query1)
                            {
                             echo '<script type="text/javascript">alert("Registration Successfully Done")</script>';
                               
                            }
                            }

                        if(isset($_POST['login']))
                        {
                            $name=$_POST['inputUserName'];
                            $password =$_POST['inputPassword'];
                            $query1=mysqli_query($link,"SELECT * FROM customer WHERE customer_name='$name' and password='$password'");
                            
                            while($row=mysqli_fetch_array($query1,MYSQLI_ASSOC)){
                                $id=$row['customer_id'];
                                $_SESSION['username']=$name;
                                $_SESSION['id']=$id;
                                echo '<script type="text/javascript">alert("login Successfully!")</script>';
                            }
                            
                        }
                        if(isset($_POST['logout']))
                        {
                            session_destroy();
                        }
                        ?>
<header id="navheader">

    <nav class="navbar navbar-expand-lg navbar-expand-md navbar-light bg-secondary">
        <a class="navbar-brand" href="home.php">Navbar</a>


        <div class="navbar-collapse">
            <li class="navbar-nav mr-auto">
                <a class="nav-link active" href="home.php">Home <span class="sr-only"></span></a>
                <a class="nav-link" href="cart.php">Cart</a>
                <a class="nav-link" href="adminlogin.php">Admin Panel</a>
                
            </li>
            <form class="form-inline my-2 my-lg-0">
                <input class="form-control  mr-sm-2" type="text" placeholder="Search">
                <button class="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
                <?php
             if(!isset($_SESSION['username'])){
        ?>
         <button type="button" class="btn bg-info ml-4" data-target="#loginmodal" data-toggle="modal">Login</button>
          
          <?php
             }
        else
        {?>
           <button class="btn bg-info"><a style="color: black" href="logout.php">Logout <?php echo $_SESSION['username']?>?</a></button>
       <?php }?>
                
                <button class="btn bg-info ml-4" type="button" data-target="#registrationModal" data-toggle="modal">Registration</button>
            </form>
        </div>
    </nav>

    <!--Modal-->
    <!--Modal Login-->

    <div class="container">
        <!-- The Modal -->
        <div class="modal fade" id="loginmodal" tabindex="-1" data-keyboard="true" data-backdrop="static">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">

                        <h1 class="modal-title">Login</h1>
                        <button class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <form action="" method="post">
                            <div class="form-group">
                                <label for="inputUserName">User Name</label>
                                <input type="text" name="inputUserName" class="form-control" placeholder="User Name">
                            </div>
                            <div class="form-group">
                                <label for="inputPassword">Password</label>
                                <input type="password" name="inputPassword" class="form-control" placeholder="Password">
                            </div>


                            <div class="modal-footer">
                                <input type="submit" name="login" class="bg-primary">
                                <button class="bg-primary" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!--Modal Registration-->

    <div class="container">
        <!-- The Modal -->
        <div class="modal fade" id="registrationModal" tabindex="-1" data-keyboard="true" data-backdrop="static">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">

                        <h1 class="modal-title">Registration</h1>
                        <button class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">

                        <form action="" method="post">
                            <div class="form-group">
                                <label for="inputUserName">User Name</label>
                                <input type="text" name="inputUserName" class="form-control" placeholder="User Name">
                            </div>
                            <div class="form-group">
                                <label for="inputPhone">Phone Number</label>
                                <input type="text" name="inputPhone" class="form-control" placeholder="Phone Number">
                            </div>
                            <div class="form-group">
                                <label for="inputEmail">Email</label>
                                <input type="text" name="inputEmail" class="form-control" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <label for="inputAddress">Address</label>
                                <input type="text" name="inputAddress" class="form-control" placeholder="Address">
                            </div>
                            <div class="form-group">
                                <label for="inputPassword">Password</label>
                                <input type="password" name="inputPassword" class="form-control" placeholder="Password">
                            </div>
                            <div class="form-group">
                                <label for="inputConfirmPassword">Confirm Password</label>
                                <input type="password" name="inputConfirmPassword" class="form-control" placeholder="Confirm Password">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="bg-primary" data-target="#loginmodal" data-toggle="modal" data-dismiss="modal">Login</button>
                                <input class="bg-primary" type="submit" name="registration">
                                <button class="bg-primary " data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>

</header>
